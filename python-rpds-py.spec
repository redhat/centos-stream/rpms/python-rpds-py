%global srcname rpds-py
%global modname rpds_py

Name:           python-rpds-py
Version:        0.17.1
Release:        %autorelease
Summary:        Python bindings to the Rust rpds crate
# Full license breakdown in LICENSES.dependencies
License:        MIT AND Apache-2.0 AND (MIT OR Apache-2.0) AND MPL-2.0
URL:            https://github.com/crate-py/rpds
Source:         %{pypi_source %{modname}}
# To create the vendor tarball:
#   tar xf %%{modname}-%%{version}.tar.gz ; pushd %%{modname}-%%{version} ; \
#   cargo vendor && tar Jcvf ../%%{modname}-%%{version}-vendor.tar.xz vendor/ ; pushd
Source:         %{modname}-%{version}-vendor.tar.xz

%if 0%{?rhel}
BuildRequires:  rust-toolset
%else
BuildRequires:  cargo-rpm-macros
%endif
BuildRequires:  dos2unix
BuildRequires:  python3-devel
BuildRequires:  python3-pytest

%global _description %{expand:
Python bindings to the Rust rpds crate.}

%description %_description

%package -n python3-%{srcname}
Summary:        %{summary}

%description -n python3-%{srcname} %_description


%prep
%autosetup -p1 -n %{modname}-%{version} %{?rhel:-a1}

# Fix line terminations
dos2unix README* LICENSE* *.pyi

# Fix for bad requirements dependency
sed -r -i 's/^file:/# &/' tests/requirements.in

%cargo_prep %{?rhel:-v vendor}
rm -f Cargo.lock


%generate_buildrequires
%pyproject_buildrequires tests/requirements.in
%if %{undefined rhel}
%cargo_generate_buildrequires
%endif


%build
export RUSTFLAGS='%{build_rustflags}'
%cargo_license_summary
%{cargo_license} > LICENSES.dependencies
%if %{defined rhel}
%cargo_vendor_manifest
%endif
%pyproject_wheel


%install
%pyproject_install

%pyproject_save_files rpds


%check
%pytest -vv


%files -n python3-%{srcname} -f %{pyproject_files}
%license LICENSE LICENSES.dependencies
%if %{defined rhel}
%license cargo-vendor.txt
%endif
%doc README.rst


%changelog
%autochangelog
